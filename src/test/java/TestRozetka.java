import io.qameta.allure.junit4.DisplayName;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestRozetka {

    WebDriver driver;
    private static final String rozetkaUrl = "https://rozetka.com.ua";
    private static final String xpathAccount = "//a[text()=' войдите в личный кабинет ']";
    private static final String email = "valeriybagno89@gmail.com";
    private static final String password = "dfkthf66446";
    private static final String incorrectEmail = "valeriy123456@gmail.com";
    private static final String incorrectPassword = "123456";
    private static final String xpathUserName = "//a[text()=' Валерий Багно ']";
    private static final String xpathErrorEmail = "//p[@class='error-message']";
    private static final String xpathErrorPassword = "//strong[text()=' Введен неверный пароль! ']";
    private static final String searchText = "HP Pavilion Gaming 17";

    @Before
    public void start() {
        System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(rozetkaUrl);
    }

    @Test
    @DisplayName("Check login with correct credentials")
    public void test1CorrectLoginUser() {

        driver.findElement(By.xpath(xpathAccount)).click();
        LoginPage loginPage = new LoginPage(driver);
        loginPage.enterCredentials(email, password);
        loginPage.clickButtonEnter();
        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathUserName)));

        HomePage homePage = new HomePage(driver);

        assertTrue(homePage.getUserName().contains("Валерий Багно"));
    }

    @Test
    @DisplayName("Check login with incorrect email")
    public void test2IncorrectEmailLoginUser() {

        driver.findElement(By.xpath(xpathAccount)).click();
        LoginPage loginPage = new LoginPage(driver);
        loginPage.enterCredentials(incorrectEmail, password);
        loginPage.clickButtonEnter();

        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathErrorEmail)));

        WebElement isPresent = driver.findElement(By.xpath(xpathErrorEmail));

        assertTrue(isPresent.isDisplayed());
    }

    @Test
    @DisplayName("Check login with incorrect password")
    public void test3IncorrectPasswordLoginUser() {

        driver.findElement(By.xpath(xpathAccount)).click();
        LoginPage loginPage = new LoginPage(driver);
        loginPage.enterCredentials(email, incorrectPassword);
        loginPage.clickButtonEnter();

        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathErrorPassword)));

        WebElement isPresent = driver.findElement(By.xpath(xpathErrorPassword));

        assertTrue(isPresent.isDisplayed());
    }

    @Test
    @DisplayName("Check button buy in certain product")
    public void test4CheckBuyCertainProduct() {

        HomePage homePage = new HomePage(driver);
        homePage.search(searchText);

        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[@class='catalog-heading']")));

        int resultQuantity = driver.findElements(By.xpath("//div[@class='goods-tile__inner']")).size();

        assertEquals(28, resultQuantity);

        new ResultSearchPage(driver).chooseCertainLaptop();

        assertTrue(new ProductPage(driver).checkButtonBuy().contains("Купить"));
    }

    @Test
    @DisplayName("Check product in wishlist in user account")
    public void test5CheckProductInWishList() {

        driver.findElement(By.xpath(xpathAccount)).click();
        LoginPage loginPage = new LoginPage(driver);
        loginPage.enterCredentials(email, password);
        loginPage.clickButtonEnter();
        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathUserName)));

        HomePage homePage = new HomePage(driver);
        homePage.search("Realme 7 Pro 8/128GB Mirror Blue");

        ResultSearchPage resultSearchPage = new ResultSearchPage(driver);
        resultSearchPage.clickWishButton();
        homePage.clickUserName();
        homePage.clickWishList();

        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@title='Мобильный телефон Realme 7 Pro 8/128GB Mirror Blue']")));
        WebElement addGoods = driver.findElement(By.xpath("//a[@title='Мобильный телефон Realme 7 Pro 8/128GB Mirror Blue']"));

        assertTrue(addGoods.getText().contains("Realme 7 Pro"));
    }

    @Test
    @DisplayName("Check search product by categories and one filter")
    public void test6SearchProductBySite() {
        
        HomePage homePage = new HomePage(driver);
        homePage.clickButtonCatalog();
        homePage.clickButtonCategoryAppliances();

        new ResultCategory(driver).clickCategoryBlenders();

        new ResultSearchPage(driver).clickButtonFilter();

        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[contains(text(), 'Блендер Mpm MBL-26 ')]")));
        WebElement goods = driver.findElement(By.xpath("//span[contains(text(), 'Блендер Mpm MBL-26 ')]"));

        assertTrue(goods.isDisplayed());
    }

    @Test
    @DisplayName("Check add product in basket")
    public void test7CheckAddProductInBasket() {

        HomePage homePage = new HomePage(driver);
        homePage.search("015794050452");

        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[@class='product__title']")));

        ProductPage productPage = new ProductPage(driver);
        productPage.clickButtonBuy();

        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[contains(text(), 'Продолжить покупки')]")));

        new OrderPage(driver).clickButtonOrder();

        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h2[@class='check-title']")));

        WebElement oneProductInBasket = driver.findElement(By.xpath("//div[@class='bill-g-i-amount' and contains(text(),'1')]"));

        assertTrue(oneProductInBasket.getText().contains("1"));
    }

    @Test
    @DisplayName("Check write comments without login user")
    public void test8CheckWriteCommentsWithoutLoginUser() {

        HomePage homePage = new HomePage(driver);
        homePage.search("Xiaomi Mi Band 5 Black");

        new ResultSearchPage(driver).clickCertainFitnessBracelet();

        new ProductPage(driver).clickFeedbackButton();

        new FeedbackPage(driver).clickButtonLeaveFeedback();

        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h3[@class='modal__heading']")));

        LoginPage loginPage = new LoginPage(driver);

        assertEquals("Войти", loginPage.checkTitleEnter());
    }

    @Test
    @DisplayName("Check leave comments with login user")
    public void test9CheckLeaveCommentsWithLoginUser() {

        driver.findElement(By.xpath(xpathAccount)).click();
        LoginPage loginPage = new LoginPage(driver);
        loginPage.enterCredentials(email, password);
        loginPage.clickButtonEnter();
        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathUserName)));

        HomePage homePage = new HomePage(driver);
        homePage.search("PHILIPS ThermoProtect Ionic HP8233/00");

        new ProductPage(driver).clickFeedbackButton();
        new FeedbackPage(driver).clickButtonLeaveFeedback();

        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h3[@class='modal__heading']")));

        CommentsPage commentsPage = new CommentsPage(driver);
        commentsPage.clickStarsProduct();
        commentsPage.enterDostoinstva("Отличный фен");
        commentsPage.enterNedostatki("-");
        commentsPage.enterComment("Отличный фен");
        commentsPage.clickLeaveComment();
        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h3[@class='modal__heading']")));

        assertEquals("Ваш отзыв будет опубликован после прохождения модерации", commentsPage.checkTextResultLeaveComment());
    }

    @Test
    @DisplayName("Check field in specifications of product")
    public void test9CheckFieldSpecifications() {

        HomePage homePage = new HomePage(driver);
        homePage.clickButtonCatalog();
        homePage.clickButtonCategoryMobile();

        ResultCategory resultCategory = new ResultCategory(driver);
        resultCategory.clickCategoryGadgets();
        resultCategory.clickCategoryWearGadgets();
        resultCategory.clickCategoryFitnessBracelets();
        resultCategory.clickCertainGadget();

        ProductPage productPage = new ProductPage(driver);
        productPage.clickSpecificationsButton();

        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h2[@class='product-tabs__heading']")));

        SpecificationsPage specificationsPage = new SpecificationsPage(driver);

        assertTrue(specificationsPage.checkFieldSpecifications().contains("Гарантия"));
    }

    @After
    public void close() {
        driver.quit();
    }
}


