package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class OrderPage extends BasePage {

    @FindBy(xpath = "//a[contains(text(), 'Оформить заказ')]")
    private WebElement buttonOrder;

    public OrderPage(WebDriver driver) {
        super(driver);
    }

    @Step
    public void clickButtonOrder() {

        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.visibilityOf(buttonOrder));
        buttonOrder.click();
    }
}
