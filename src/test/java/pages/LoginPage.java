package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {

    @FindBy(xpath = "//h3[@class='modal__heading']")
    private WebElement titleEnter;

    @FindBy(xpath = "//input[@id='auth_email']")
    private WebElement emailField;

    @FindBy(xpath = "//input[@id='auth_pass']")
    private WebElement passwordField;

    @FindBy(xpath = "//button[text()=' Войти ']")
    private WebElement buttonEnter;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    @Step
    public void enterCredentials(String email, String password) {

        emailField.sendKeys(email);
        passwordField.sendKeys(password);
    }

    @Step
    public void clickButtonEnter() {

        buttonEnter.click();
    }

    @Step
    public String checkTitleEnter() {

        return titleEnter.getText();
    }
}
