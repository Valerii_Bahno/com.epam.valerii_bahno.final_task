package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SpecificationsPage extends BasePage {

    @FindBy(xpath = "//span[text()='Гарантия']")
    private WebElement fieldSpecifications;

    public SpecificationsPage(WebDriver driver) {
        super(driver);
    }

    @Step
    public String checkFieldSpecifications() {

        return fieldSpecifications.getText();
    }
}
