package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FeedbackPage extends BasePage {

    @FindBy(xpath = "//button[contains(text(), 'Оставить отзыв')]")
    private WebElement buttonLeaveFeedback;

    public FeedbackPage(WebDriver driver) {
        super(driver);
    }

    @Step
    public void clickButtonLeaveFeedback() {

        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.visibilityOf(buttonLeaveFeedback));
        buttonLeaveFeedback.click();
    }
}
