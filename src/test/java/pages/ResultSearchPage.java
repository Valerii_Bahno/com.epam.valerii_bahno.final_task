package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ResultSearchPage extends BasePage{

    @FindBy(xpath = "//span[contains(text(),'1A8P8EA')]")
    private WebElement certainLaptop;

    @FindBy(css = "app-goods-wishlist .wish-button_state_active")
    private WebElement wishButton;

    @FindBy(xpath = "//label[@for='Вакуумный']")
    private WebElement buttonFilter;

    @FindBy(xpath = "//span[contains(text(), 'XMSH10HM')]")
    private WebElement certainFitnessBracelet;

    public ResultSearchPage(WebDriver driver) {
        super(driver);
    }

    @Step
    public void chooseCertainLaptop() {

        certainLaptop.click();
    }

    @Step
    public void clickWishButton() {

        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.visibilityOf(wishButton));
        wishButton.click();
    }

    @Step
    public void clickButtonFilter() {

        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.visibilityOf(buttonFilter));
        buttonFilter.click();
    }

    @Step
    public void clickCertainFitnessBracelet() {

        new WebDriverWait(driver, 20, 500)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[contains(text(), 'Найдено')]")));
        certainFitnessBracelet.click();
    }
}
