package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CommentsPage extends BasePage {

    @FindBy(xpath = "//li[contains(text(), 'Отличный')]")
    private WebElement iconFiveStars;

    @FindBy(xpath = "//input[@id='dostoinstva']")
    private WebElement fieldDostoinstva;

    @FindBy(xpath = "//input[@id='nedostatki']")
    private WebElement fieldNedostatki;

    @FindBy(xpath = "//textarea[@id='comment-text']")
    private WebElement fieldComment;

    @FindBy(xpath = "//div[@class='form__row']//button[contains(text(),' Оставить отзыв')]")
    private WebElement buttonLeaveComment;

    @FindBy(xpath = "//p[contains(text(),'Ваш отзыв будет опубликован')]")
    private WebElement resultLeaveComment;

    public CommentsPage(WebDriver driver) {
        super(driver);
    }

    @Step
    public void clickStarsProduct() {
        iconFiveStars.click();
    }

    @Step
    public void enterDostoinstva(String textDostoinstva) {

        fieldDostoinstva.sendKeys(textDostoinstva);
    }

    @Step
    public void enterNedostatki(String textNedostatki) {

        fieldNedostatki.sendKeys(textNedostatki);
    }

    @Step
    public void enterComment(String textComment) {

        fieldComment.sendKeys(textComment);
    }

    @Step
    public void clickLeaveComment() {

        buttonLeaveComment.click();
    }

    @Step
    public String checkTextResultLeaveComment() {

        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//p[@class='thanks-modal__message']")));
        return resultLeaveComment.getText();
    }
}
