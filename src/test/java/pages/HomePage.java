package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage extends BasePage {

    @FindBy(xpath = "//p[@class='header-topline__user-text']//a[@href='https://rozetka.com.ua/cabinet/personal-information']")
    private WebElement userName;

    @FindBy(xpath = "//input[@name='search']")
    private WebElement searchField;

    @FindBy(xpath = "//button[@class='menu-toggler']")
    private WebElement buttonCatalog;

    @FindBy(xpath = "//a[@href='https://rozetka.com.ua/cabinet/wishlist']")
    private WebElement wishList;

    @FindBy(xpath = "//a[contains(text(), 'Смартфоны, ТВ и электроника')]")
    private WebElement buttonCategoryMobile;

    @FindBy(xpath = "//a[contains(text(), 'Бытовая техника')]")
    private WebElement buttonCategoryAppliances;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public String getUserName() {

        return userName.getText();
    }

    @Step
    public ResultSearchPage search(String searchText) {

        searchField.sendKeys(searchText, Keys.ENTER);
        return new ResultSearchPage(driver);
    }

    @Step
    public void clickUserName() {

        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.visibilityOf(userName));
        userName.click();
    }

    @Step
    public void clickWishList() {

        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.visibilityOf(wishList));
        wishList.click();
    }

    @Step
    public void clickButtonCatalog() {

        buttonCatalog.click();
    }

    @Step
    public void clickButtonCategoryMobile() {

        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.visibilityOf(buttonCategoryMobile));
        buttonCategoryMobile.click();
    }

    @Step
    public void clickButtonCategoryAppliances() {

        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.visibilityOf(buttonCategoryAppliances));
        buttonCategoryAppliances.click();
    }
}
