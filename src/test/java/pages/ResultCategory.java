package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ResultCategory extends BasePage {

    @FindBy(xpath = "//a[contains(text(), 'Телефоны, наушники, GPS')]")
    private WebElement categoryGadgets;

    @FindBy(xpath = "//span[contains(text(), 'Носимые гаджеты')]")
    private WebElement categoryWearGadgets;

    @FindBy(xpath = "//span[contains(text(), 'Фитнес-браслеты')]")
    private WebElement categoryFitnessBracelets;

    @FindBy(xpath = "//span[contains(text(),'Huawei Band 3 Pro Blue GPS')]")
    private WebElement certainGadget;

    @FindBy(xpath = "//a[@title='Блендеры']")
    private WebElement categoryBlenders;

    public ResultCategory(WebDriver driver) {
        super(driver);
    }

    @Step
    public void clickCategoryGadgets() {

        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.visibilityOf(categoryGadgets));
        categoryGadgets.click();
    }

    @Step
    public void clickCategoryWearGadgets() {

        new WebDriverWait(driver, 40, 500)
                .until(ExpectedConditions.visibilityOf(categoryWearGadgets));
        categoryWearGadgets.click();
    }

    @Step
    public void clickCategoryFitnessBracelets() {

        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.visibilityOf(categoryFitnessBracelets));
        categoryFitnessBracelets.click();
    }

    @Step
    public void clickCertainGadget() {

        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.visibilityOf(certainGadget));
        certainGadget.click();
    }

    @Step
    public void clickCategoryBlenders() {

        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.visibilityOf(categoryBlenders));
        categoryBlenders.click();
    }
}
