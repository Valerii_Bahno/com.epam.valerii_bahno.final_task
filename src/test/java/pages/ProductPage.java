package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProductPage extends BasePage {

    @FindBy(xpath = "//app-buy-button[@usershash='buy']//button[@aria-label='Купить']")
    private WebElement buttonBuy;

    @FindBy(xpath = "//a[contains(text(),'Характеристики')]")
    private WebElement specificationsButton;

    @FindBy(xpath = "//a[contains(text(), 'Отзывы')]")
    private WebElement feedbackButton;

    public ProductPage(WebDriver driver) {
        super(driver);
    }

    @Step
    public String checkButtonBuy() {

        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.visibilityOf(buttonBuy));
        return buttonBuy.getText();
    }

    @Step
    public void clickButtonBuy() {

        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.visibilityOf(buttonBuy));
        buttonBuy.click();
    }

    @Step
    public void clickSpecificationsButton() {

        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.visibilityOf(specificationsButton));
        specificationsButton.click();
    }

    @Step
    public void clickFeedbackButton() {

        new WebDriverWait(driver, 30, 500)
                .until(ExpectedConditions.visibilityOf(feedbackButton));
        feedbackButton.click();
    }
}
